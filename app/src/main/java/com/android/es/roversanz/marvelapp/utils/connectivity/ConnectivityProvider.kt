package com.android.es.roversanz.marvelapp.utils.connectivity

import android.content.Context

interface ConnectivityProvider {

    fun isOnline(): Boolean

    fun isWifiConnected(): Boolean

}

class ConnectivityProviderImpl(private val context: Context) : ConnectivityProvider {

    override fun isOnline() = context.isOnline()

    override fun isWifiConnected() = context.isWifiConnected()

}
