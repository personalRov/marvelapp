package com.android.es.roversanz.marvelapp.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.app
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.scopes.ActivityScope
import com.android.es.roversanz.marvelapp.ui.detail.DetailCharacterFragment
import com.android.es.roversanz.marvelapp.ui.detail.DetailComicFragment
import com.android.es.roversanz.marvelapp.ui.list.ListCharacterFragment
import com.android.es.roversanz.marvelapp.ui.list.ListComicFragment
import com.android.es.roversanz.marvelapp.ui.list.ListComicFragmentContract
import dagger.Component
import dagger.Module
import dagger.Provides
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainActivityContract.MainActivityView,
        ListCharacterFragment.Callback,
        ListComicFragment.Callback,
        DetailCharacterFragment.Callback {

    companion object {
        private val LIST_FRAGMENT_CHARACTERS = "LIST_FRAGMENT_CHARACTERS"
        private val LIST_FRAGMENT_COMICS = "LIST_FRAGMENT_COMICS"
        private val DETAIL_CHARACTER_FRAGMENT = "DETAIL_CHARACTER_FRAGMENT"
        private val DETAIL_COMIC_FRAGMENT = "DETAIL_COMIC_FRAGMENT"
    }

    @Inject lateinit var presenter: MainActivityContract.MainActivityPresenter

    private var loading: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inject(this.app())
        presenter.attach(this)
        presenter.prepareView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun showLoading() {
        if (loading == null) {
            loading = indeterminateProgressDialog("Loading")
        }
        loading?.show()
    }

    override fun hideLoading() {
        loading?.hide()
    }

    override fun showError(message: String?) {
        message
                ?.let { alert(it, getString(R.string.error_title)).show() }
                ?: alert(getString(R.string.error_generic), getString(R.string.error_title)).show()
        hideLoading()
    }

    override fun showCharactersListItems(items: List<Character>) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, ListCharacterFragment.Companion.newBundle(items), LIST_FRAGMENT_CHARACTERS)
                .commit()
    }

    override fun showCharacterDetail(character: Character) {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, DetailCharacterFragment.Companion.newBundle(character), DETAIL_CHARACTER_FRAGMENT)
                .addToBackStack(DETAIL_CHARACTER_FRAGMENT)
                .commit()
    }

    override fun onRefreshCharactersItems(items: List<Character>) {
        val fr = supportFragmentManager.findFragmentByTag(LIST_FRAGMENT_CHARACTERS)
        fr?.let {
            (it as ListCharacterFragmentContract.ListCharacterFragmentView).onRefreshItems(items)
        }
    }

    override fun refreshItems(offset: Int) {
        presenter.refreshItems(offset)
    }

    override fun onCharacterSelected(character: Character) {
        presenter.onCharacterSelected(character)
    }

    override fun showComicsListItems(items: List<Comic>) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, ListComicFragment.Companion.newBundle(items), LIST_FRAGMENT_COMICS)
                .commit()
    }

    override fun showComicDetail(comic: Comic) {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, DetailComicFragment.Companion.newBundle(comic), DETAIL_COMIC_FRAGMENT)
                .addToBackStack(DETAIL_COMIC_FRAGMENT)
                .commit()
    }

    override fun onRefreshComicsItems(items: List<Comic>) {
        val fr = supportFragmentManager.findFragmentByTag(LIST_FRAGMENT_COMICS)
        fr?.let {
            (it as ListComicFragmentContract.ListComicFragmentView).onRefreshItems(items)
        }
    }

    override fun onComicSelected(comic: Comic) {
        presenter.onComicSelected(comic)
    }

    private fun inject(application: ApplicationProject) {
        DaggerMainActivity_MainActivityComponent.builder()
                .mainComponent(application.component)
                .build()
                .inject(this)
    }

    @ActivityScope
    @Component(dependencies = arrayOf(MainComponent::class), modules = arrayOf(MainActivityModule::class))
    internal interface MainActivityComponent {

        fun inject(activity: MainActivity)
    }

    @Module
    @ActivityScope
    internal class MainActivityModule {

        @Provides
        fun provideMainActivityPresenter(marvelRepository: MarvelRepository): MainActivityContract.MainActivityPresenter
                = MainActivityPresenterImpl(marvelRepository)

    }

}
