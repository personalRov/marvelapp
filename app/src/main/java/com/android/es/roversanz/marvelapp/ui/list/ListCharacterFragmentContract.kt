package com.android.es.roversanz.marvelapp.ui

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.ui.base.BaseContract

interface ListCharacterFragmentContract {

    interface ListCharacterFragmentPresenter : BaseContract.BasePresenter<ListCharacterFragmentView> {
        fun prepareView(list: List<Character>)
        fun onRefreshItems(items: List<Character>)
        fun onRefresh()
        fun onRefresh(itemCount: Int)
    }

    interface ListCharacterFragmentView : BaseContract.BaseViewFragment {
        fun onRefreshItems(items: List<Character>)

        fun showNoItemsVisibility(visibility: Boolean)
        fun refreshItems(itemCount: Int)
        fun itemsUpdated(items: List<Character>)
    }
}
