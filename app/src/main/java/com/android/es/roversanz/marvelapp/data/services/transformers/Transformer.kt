package com.android.es.roversanz.marvelapp.data.services.transformers

import com.android.es.roversanz.marvelapp.data.model.MarvelEntity
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelContractEntity
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse

interface Transformer<in OC : MarvelContractEntity, out T : MarvelEntity> {
    companion object {
        val DOT = "."
        val IMAGE_MEDIUM_SIZE = "/standard_medium"
        val IMAGE_BIG_SIZE = "/landscape_xlarge"
    }

    fun transformer(outputContract: MarvelResponse<OC>): List<T>
}
