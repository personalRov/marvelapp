package com.android.es.roversanz.marvelapp.ui.detail

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.scopes.ActivityScope
import com.android.es.roversanz.marvelapp.di.scopes.FragmentScope
import com.android.es.roversanz.marvelapp.ui.base.BaseFragment
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import com.bumptech.glide.Glide
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.fragment_character_detail.character_comics_count
import kotlinx.android.synthetic.main.fragment_character_detail.character_date
import kotlinx.android.synthetic.main.fragment_character_detail.character_description
import kotlinx.android.synthetic.main.fragment_character_detail.character_events_count
import kotlinx.android.synthetic.main.fragment_character_detail.character_image
import kotlinx.android.synthetic.main.fragment_character_detail.character_name
import kotlinx.android.synthetic.main.fragment_character_detail.character_series_count
import kotlinx.android.synthetic.main.fragment_character_detail.character_stories_count
import org.parceler.Parcels

class DetailCharacterFragment : BaseFragment<DetailCharacterFragmentContract.DetailCharacterFragmentView, DetailCharacterFragmentContract.DetailCharacterFragmentPresenter, DetailCharacterFragment.Callback>(),
        DetailCharacterFragmentContract.DetailCharacterFragmentView {

    companion object {
        private val KEY_CHARACTER = "key:character"

        fun newBundle(character: Character) = DetailCharacterFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_CHARACTER, Parcels.wrap(character))
            }
        }
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val character = Parcels.unwrap<Character>(arguments?.get(KEY_CHARACTER) as Parcelable?)
        character?.let {
            presenter.prepareView(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }

    override fun setCharacterName(message: String) {
        character_name.text = message
    }

    override fun setCharacterDescription(message: String) {
        character_description.text = message
    }

    override fun setCharacterDate(message: String) {
        character_date.text = message
    }

    override fun loadCharacterImage(image: String) {
        Glide.with(context)
                .load(image)
                .into(character_image)
    }

    override fun setCharacterComics(message: String) {
        character_comics_count.text = message
    }

    override fun setCharacterStories(message: String) {
        character_stories_count.text = message
    }

    override fun setCharacterEvents(message: String) {
        character_events_count.text = message
    }

    override fun setCharacterSeries(message: String) {
        character_series_count.text = message
    }

    override fun getFragmentLayout() = R.layout.fragment_character_detail

    override fun inject(application: ApplicationProject) {
        DaggerDetailCharacterFragment_DetailCharacterFragmentComponent.builder()
                .mainComponent(application.component)
                .build()
                .inject(this)
    }

    interface Callback : BaseFragment.Callback

    @FragmentScope
    @Component(dependencies = arrayOf(MainComponent::class), modules = arrayOf(DetailCharacterFragmentModule::class))
    internal interface DetailCharacterFragmentComponent {

        fun inject(fragment: DetailCharacterFragment)

    }

    @Module
    @ActivityScope
    internal class DetailCharacterFragmentModule {

        @Provides
        fun provideDetailCharacterFragmentPresenter(stringProvider: StringProvider): DetailCharacterFragmentContract.DetailCharacterFragmentPresenter
                = DetailCharacterFragmentPresenterImpl(stringProvider)

    }

}
