package com.android.es.roversanz.marvelapp.ui.base

interface BaseContract {

    interface BasePresenter<in T : BaseView> {
        fun attach(view: T)

        fun detach()

        fun isAttached():Boolean
    }

    interface BaseView

    interface BaseViewActivity : BaseView {

        fun showLoading()

        fun hideLoading()

        fun showError(message: String?)
    }

    interface BaseViewFragment : BaseView

}
