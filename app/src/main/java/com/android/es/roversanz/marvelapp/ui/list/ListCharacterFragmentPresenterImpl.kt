package com.android.es.roversanz.marvelapp.ui

import android.support.annotation.VisibleForTesting
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.ui.base.BasePresenterImpl
import javax.inject.Inject

class ListCharacterFragmentPresenterImpl @Inject constructor() :
        BasePresenterImpl<ListCharacterFragmentContract.ListCharacterFragmentView>(),
        ListCharacterFragmentContract.ListCharacterFragmentPresenter {

    @VisibleForTesting
    var items: MutableList<Character> = mutableListOf()

    override fun prepareView(list: List<Character>) {
        this.items.clear()
        this.items.addAll(list)
        view?.showNoItemsVisibility(items.isEmpty())
        view?.itemsUpdated(items)
    }

    override fun onRefresh() {
        this.items.clear()
        view?.refreshItems(0)
    }

    override fun onRefresh(itemCount: Int) {
        view?.refreshItems(itemCount)
    }

    override fun onRefreshItems(list: List<Character>) {
        this.items.addAll(list)
        view?.showNoItemsVisibility(items.isEmpty())
        view?.itemsUpdated(items)
    }

}
