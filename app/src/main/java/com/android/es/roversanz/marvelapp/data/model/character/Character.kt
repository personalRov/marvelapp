package com.android.es.roversanz.marvelapp.data.model.character

import com.android.es.roversanz.marvelapp.data.model.MarvelEntity
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import org.parceler.Parcel
import org.parceler.ParcelConstructor
import java.util.Date

@Parcel(Parcel.Serialization.BEAN)
data class Character @ParcelConstructor constructor(
        val id: Int,
        val name: String,
        val description: String,
        val modifiedDate: Date?,
        val resourceURI: String,
        val thumbnail: String,
        val picture: String,
        val comicsSummary: SummaryList,
        val seriesSummary: SummaryList,
        val storiesSummary: SummaryList,
        val eventsSummary: SummaryList) : MarvelEntity
