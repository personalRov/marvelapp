package com.android.es.roversanz.marvelapp.ui.detail

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.scopes.ActivityScope
import com.android.es.roversanz.marvelapp.di.scopes.FragmentScope
import com.android.es.roversanz.marvelapp.ui.base.BaseFragment
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import com.bumptech.glide.Glide
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_creators_count
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_date
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_description
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_events_count
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_image
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_series_count
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_stories_count
import kotlinx.android.synthetic.main.fragment_comic_detail.comic_title
import org.parceler.Parcels

class DetailComicFragment : BaseFragment<DetailComicFragmentContract.DetailComicFragmentView, DetailComicFragmentContract.DetailComicFragmentPresenter, DetailComicFragment.Callback>(),
        DetailComicFragmentContract.DetailComicFragmentView {

    companion object {
        private val KEY_COMIC = "key:comic"

        fun newBundle(comic: Comic) = DetailComicFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_COMIC, Parcels.wrap(comic))
            }
        }
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val comic = Parcels.unwrap<Comic>(arguments?.get(KEY_COMIC) as Parcelable?)
        comic?.let {
            presenter.prepareView(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }

    override fun setComicTitle(message: String) {
        comic_title.text = message
    }

    override fun setComicDescription(message: String) {
        comic_description.text = message
    }

    override fun setComicDate(message: String) {
        comic_date.text = message
    }

    override fun loadComicImage(image: String) {
        Glide.with(context)
                .load(image)
                .into(comic_image)
    }

    override fun setComicCreators(message: String) {
        comic_creators_count.text = message
    }

    override fun setComicStories(message: String) {
        comic_stories_count.text = message
    }

    override fun setComicEvents(message: String) {
        comic_events_count.text = message
    }

    override fun setComicCharacters(message: String) {
        comic_series_count.text = message
    }

    override fun getFragmentLayout() = R.layout.fragment_comic_detail

    override fun inject(application: ApplicationProject) {
        DaggerDetailComicFragment_DetailComicFragmentComponent.builder()
                .mainComponent(application.component)
                .build()
                .inject(this)
    }

    interface Callback : BaseFragment.Callback

    @FragmentScope
    @Component(dependencies = arrayOf(MainComponent::class), modules = arrayOf(DetailComicFragmentModule::class))
    internal interface DetailComicFragmentComponent {

        fun inject(fragment: DetailComicFragment)

    }

    @Module
    @ActivityScope
    internal class DetailComicFragmentModule {

        @Provides
        fun provideDetailComicFragmentPresenter(stringProvider: StringProvider): DetailComicFragmentContract.DetailComicFragmentPresenter
                = DetailComicFragmentPresenterImpl(stringProvider)

    }

}
