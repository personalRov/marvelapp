package com.android.es.roversanz.marvelapp.ui.list

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.android.es.roversanz.marvelapp.ApplicationProject
import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.scopes.ActivityScope
import com.android.es.roversanz.marvelapp.di.scopes.FragmentScope
import com.android.es.roversanz.marvelapp.ui.ListCharacterFragmentContract
import com.android.es.roversanz.marvelapp.ui.ListCharacterFragmentPresenterImpl
import com.android.es.roversanz.marvelapp.ui.base.BaseFragment
import com.android.es.roversanz.marvelapp.utils.EndlessRecyclerViewScrollListener
import com.android.es.roversanz.marvelapp.utils.adapters.CharactersAdapter
import com.android.es.roversanz.marvelapp.utils.adapters.CharactersAdapterListener
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.fragment_character_list.characters_empty
import kotlinx.android.synthetic.main.fragment_character_list.characters_swipe
import kotlinx.android.synthetic.main.fragment_character_list.rv_characters
import org.parceler.Parcels

class ListCharacterFragment : BaseFragment<ListCharacterFragmentContract.ListCharacterFragmentView, ListCharacterFragmentContract.ListCharacterFragmentPresenter, ListCharacterFragment.Callback>(),
        SwipeRefreshLayout.OnRefreshListener,
        ListCharacterFragmentContract.ListCharacterFragmentView,
        CharactersAdapterListener {

    companion object {
        private val KEY_CHARACTERS = "key:character_list"

        fun newBundle(items: List<Character>) = ListCharacterFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_CHARACTERS, Parcels.wrap(items))
            }
        }
    }

    private lateinit var adapter: CharactersAdapter

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        configureView()
        val list = Parcels.unwrap<List<Character>>(arguments?.getParcelable(KEY_CHARACTERS))
        presenter.prepareView(list)
    }

    override fun onRefresh() {
        presenter.onRefresh()
    }

    override fun onRefreshItems(items: List<Character>) {
        presenter.onRefreshItems(items)
    }

    override fun itemsUpdated(items: List<Character>) {
        adapter.update(items)
        characters_swipe.isRefreshing = false
    }

    override fun showNoItemsVisibility(visibility: Boolean) {
        characters_empty.visibility = if (visibility) View.VISIBLE else View.GONE
    }

    override fun refreshItems(itemCount: Int) {
        callback.refreshItems(itemCount)
    }

    override fun onCharacterSelected(character: Character) {
        callback.onCharacterSelected(character)
    }

    private fun configureView() {
        characters_swipe.setOnRefreshListener(this)

        val llm = LinearLayoutManager(context)
        rv_characters.layoutManager = llm
        rv_characters.setHasFixedSize(true)
        adapter = CharactersAdapter(this)
        rv_characters.adapter = adapter
        rv_characters.addOnScrollListener(object : EndlessRecyclerViewScrollListener(llm) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.onRefresh(adapter.itemCount)
            }

        })
    }

    override fun getFragmentLayout() = R.layout.fragment_character_list

    override fun inject(application: ApplicationProject) {
        DaggerListCharacterFragment_ListCharacterFragmentComponent.builder()
                .mainComponent(application.component)
                .build()
                .inject(this)
    }

    interface Callback : BaseFragment.Callback {
        fun refreshItems(offset: Int)
        fun onCharacterSelected(character: Character)
    }

    @FragmentScope
    @Component(dependencies = arrayOf(MainComponent::class), modules = arrayOf(ListCharacterFragmentModule::class))
    internal interface ListCharacterFragmentComponent {

        fun inject(fragment: ListCharacterFragment)

    }

    @Module
    @ActivityScope
    internal class ListCharacterFragmentModule {

        @Provides
        fun provideListFragmentPresenter(): ListCharacterFragmentContract.ListCharacterFragmentPresenter = ListCharacterFragmentPresenterImpl()

    }

}
