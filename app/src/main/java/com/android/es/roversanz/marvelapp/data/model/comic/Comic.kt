package com.android.es.roversanz.marvelapp.data.model.comic

import com.android.es.roversanz.marvelapp.data.model.DateItem
import com.android.es.roversanz.marvelapp.data.model.MarvelEntity
import com.android.es.roversanz.marvelapp.data.model.PriceItem
import com.android.es.roversanz.marvelapp.data.model.SummaryItem
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.TextItem
import com.android.es.roversanz.marvelapp.data.model.UrlItem
import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
data class Comic @ParcelConstructor constructor(
        val id: Int,
        val digitalId: Int,
        val title: String,
        val issueNumber: Int,
        val variantDescription: String,
        val description: String,
        val modifiedDate: String?,
        val isbn: String,
        val upc: String,
        val diamondCode: String,
        val ean: String,
        val issn: String,
        val format: String,
        val pageCount: Int,
        val resourceURI: String,
        val thumbnail: String,
        val picture: String,
        val textObjects: List<TextItem>,
        val urls: List<UrlItem>,
        val prices: List<PriceItem>,
        val dates: List<DateItem>,
        val images: List<String>,
        val serie: SummaryItem?,
        val variants: List<SummaryItem>,
        val collections: List<SummaryItem>,
        val collectedIssues: List<SummaryItem>,
        val charactersSummary: SummaryList,
        val creatorsSummary: SummaryList,
        val storiesSummary: SummaryList,
        val eventsSummary: SummaryList) : MarvelEntity
