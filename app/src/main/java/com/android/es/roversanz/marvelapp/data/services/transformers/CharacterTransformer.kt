package com.android.es.roversanz.marvelapp.data.services.transformers

import com.android.es.roversanz.marvelapp.data.model.SummaryItem
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelCharacterOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse
import com.android.es.roversanz.marvelapp.data.services.transformers.Transformer.Companion.DOT
import com.android.es.roversanz.marvelapp.data.services.transformers.Transformer.Companion.IMAGE_BIG_SIZE
import com.android.es.roversanz.marvelapp.data.services.transformers.Transformer.Companion.IMAGE_MEDIUM_SIZE

class CharacterTransformer : Transformer<MarvelCharacterOutputContract, Character> {

    override fun transformer(outputContract: MarvelResponse<MarvelCharacterOutputContract>): List<Character> =
            outputContract.data?.results?.map { c ->
                Character(
                        id = c.id ?: 0,
                        name = c.name ?: "",
                        description = c.description ?: "",
                        resourceURI = c.resourceURI ?: "",
                        modifiedDate = c.modified,
                        thumbnail = c.thumbnail?.let { it.path + IMAGE_MEDIUM_SIZE + DOT + it.extension } ?: "",
                        picture = c.thumbnail?.let { it.path + IMAGE_BIG_SIZE + DOT + it.extension } ?: "",
                        comicsSummary = SummaryList(
                                available = c.comics?.available ?: 0,
                                returned = c.comics?.returned ?: 0,
                                collectionURI = c.comics?.collectionURI ?: "",
                                items = c.comics?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        seriesSummary = SummaryList(
                                available = c.series?.available ?: 0,
                                returned = c.series?.returned ?: 0,
                                collectionURI = c.series?.collectionURI ?: "",
                                items = c.series?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        storiesSummary = SummaryList(
                                available = c.stories?.available ?: 0,
                                returned = c.stories?.returned ?: 0,
                                collectionURI = c.stories?.collectionURI ?: "",
                                items = c.stories?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        eventsSummary = SummaryList(
                                available = c.events?.available ?: 0,
                                returned = c.events?.returned ?: 0,
                                collectionURI = c.events?.collectionURI ?: "",
                                items = c.events?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList())
                         )

            } ?: emptyList()

}
