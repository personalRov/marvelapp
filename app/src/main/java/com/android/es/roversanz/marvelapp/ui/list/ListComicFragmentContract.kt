package com.android.es.roversanz.marvelapp.ui.list

import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.base.BaseContract

interface ListComicFragmentContract {

    interface ListComicFragmentPresenter : BaseContract.BasePresenter<ListComicFragmentView> {
        fun prepareView(list: List<Comic>)
        fun onRefreshItems(items: List<Comic>)
        fun onRefresh()
        fun onRefresh(itemCount: Int)
    }

    interface ListComicFragmentView : BaseContract.BaseViewFragment {
        fun onRefreshItems(items: List<Comic>)

        fun showNoItemsVisibility(visibility:Boolean)
        fun refreshItems(itemCount: Int)
        fun itemsUpdated(items: List<Comic>)
    }
}
