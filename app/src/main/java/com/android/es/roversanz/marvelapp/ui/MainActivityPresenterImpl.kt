package com.android.es.roversanz.marvelapp.ui

import android.support.annotation.VisibleForTesting
import com.android.es.roversanz.marvelapp.BuildConfig.MARVEL_TYPE
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.CharacterListener
import com.android.es.roversanz.marvelapp.data.services.ComicListener
import com.android.es.roversanz.marvelapp.data.services.MarvelRepository
import com.android.es.roversanz.marvelapp.ui.base.BasePresenterImpl
import javax.inject.Inject

class MainActivityPresenterImpl @Inject constructor(
        private val marvelRepository: MarvelRepository) :
        BasePresenterImpl<MainActivityContract.MainActivityView>(),
        MainActivityContract.MainActivityPresenter {

    companion object {
        private val TYPE_COMIC = "COMIC"
        private val TYPE_CHARACTER = "CHARACTER"
    }

    @VisibleForTesting
    var firstTime: Boolean = true

    override fun prepareView() {
        refreshItems(0)
    }

    override fun refreshItems(offset: Int) {
        view?.showLoading()
        when (MARVEL_TYPE) {
            TYPE_COMIC     -> marvelRepository.getComics(object : ComicListener {
                override fun onSuccess(list: List<Comic>) {
                    if (firstTime) {
                        view?.showComicsListItems(list)
                        firstTime = false
                    } else {
                        view?.onRefreshComicsItems(list)
                    }
                    view?.hideLoading()
                }

                override fun onError(message: String?) {
                    view?.showError(message)
                    view?.hideLoading()
                }

            }, offset)
            TYPE_CHARACTER -> marvelRepository.getCharacters(object : CharacterListener {
                override fun onSuccess(list: List<Character>) {
                    if (firstTime) {
                        view?.showCharactersListItems(list)
                        firstTime = false
                    } else {
                        view?.onRefreshCharactersItems(list)
                    }
                    view?.hideLoading()
                }

                override fun onError(message: String?) {
                    view?.showError(message)
                    view?.hideLoading()
                }
            }, offset)
        }
    }

    override fun onCharacterSelected(character: Character) {
        view?.showCharacterDetail(character)
    }

    override fun onComicSelected(comic: Comic) {
        view?.showComicDetail(comic)
    }

}
