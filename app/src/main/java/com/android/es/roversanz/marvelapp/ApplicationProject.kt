package com.android.es.roversanz.marvelapp

import android.app.Activity
import android.app.Application
import com.android.es.roversanz.marvelapp.di.components.DaggerMainComponent
import com.android.es.roversanz.marvelapp.di.components.MainComponent
import com.android.es.roversanz.marvelapp.di.modules.ApplicationModule

fun Activity.app() = application as ApplicationProject

class ApplicationProject : Application() {

    var component: MainComponent? = null

    override fun onCreate() {
        super.onCreate()
        initializeInjector()
    }

    private fun initializeInjector() {
        component = DaggerMainComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}
