package com.android.es.roversanz.marvelapp.ui.base

import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class BasePresenterImplTest {

    private lateinit var presenter: BasePresenterImpl<BaseContract.BaseView>

    @Before
    fun setUp() {

        presenter = object : BasePresenterImpl<BaseContract.BaseView>() {}
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testIsAttached_True() {
        val view = PowerMock.createMock(BaseContract.BaseView::class.java)

        PowerMock.replayAll()

        presenter.attach(view)
        Truth.assertThat(presenter.isAttached()).isTrue()
    }

    @Test
    fun testIsAttached_False() {
        val view = PowerMock.createMock(BaseContract.BaseView::class.java)
        PowerMock.replayAll()

        presenter.view = view
        presenter.detach()
        Truth.assertThat(presenter.isAttached()).isFalse()
    }

}
