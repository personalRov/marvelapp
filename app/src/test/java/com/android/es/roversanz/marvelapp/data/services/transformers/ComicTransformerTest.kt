package com.android.es.roversanz.marvelapp.data.services.transformers

import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.data.services.contracts.DateItemResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.ItemResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.ItemResponseList
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelComicOutputContract
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelData
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.MarvelThumbnail
import com.android.es.roversanz.marvelapp.data.services.contracts.PriceItemResponse
import com.android.es.roversanz.marvelapp.data.services.contracts.TextObject
import com.android.es.roversanz.marvelapp.data.services.contracts.UrlItemResponse
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class ComicTransformerTest {

    private lateinit var transformer: ComicTransformer

    @Before
    fun setUp() {
        transformer = ComicTransformer()
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testTransformer_empty() {
        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData<MarvelComicOutputContract>(offset = 2, count = 4, results = emptyList())
                                           )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        Truth.assertThat(list.size).isEqualTo(0)
    }

    @Test
    fun testTransformer_notEmpty_ItemsEmpty() {
        val results = arrayListOf<MarvelComicOutputContract>()
        results.add(MarvelComicOutputContract(
                id = 1,
                digitalId = 1,
                title = "title",
                issueNumber = 1,
                variantDescription = "variantDescription",
                modified = "modified",
                isbn = "isbn",
                upc = "upc",
                diamondCode = "diamondCode",
                ean = "ean",
                issn = "issn",
                format = "format",
                description = "description",
                pageCount = 1,
                resourceURI = "resourceURI",
                series = ItemResponse(name = "name", type = "type", role = "role", resourceURI = "resourceURI"),
                thumbnail = MarvelThumbnail(path = "path", extension = "extension"),
                textObjects = emptyList(),
                urls = emptyList(),
                variants = emptyList(),
                collections = emptyList(),
                collectedIssues = emptyList(),
                dates = emptyList(),
                prices = emptyList(),
                images = emptyList(),
                characters = ItemResponseList(available = 1, returned = 1, collectionURI = "collectionURI1", items = emptyList()),
                creators = ItemResponseList(available = 2, returned = 2, collectionURI = "collectionURI2", items = emptyList()),
                stories = ItemResponseList(available = 3, returned = 3, collectionURI = "collectionURI3", items = emptyList()),
                events = ItemResponseList(available = 4, returned = 4, collectionURI = "collectionURI4", items = emptyList()))
                   )


        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData(offset = 2, count = 4, results = results)
                                           )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        Truth.assertThat(list.size).isEqualTo(1)
        val comic: Comic = list[0]
        Truth.assertThat(comic.id).isEqualTo(1)
        Truth.assertThat(comic.title).isEqualTo("title")
        Truth.assertThat(comic.thumbnail).isEqualTo("path/standard_medium.extension")
        Truth.assertThat(comic.picture).isEqualTo("path/landscape_xlarge.extension")
        Truth.assertThat(comic.issueNumber).isEqualTo(1)
        Truth.assertThat(comic.variantDescription).isEqualTo("variantDescription")
        Truth.assertThat(comic.modifiedDate).isEqualTo("modified")
        Truth.assertThat(comic.isbn).isEqualTo("isbn")
        Truth.assertThat(comic.upc).isEqualTo("upc")
        Truth.assertThat(comic.diamondCode).isEqualTo("diamondCode")
        Truth.assertThat(comic.ean).isEqualTo("ean")
        Truth.assertThat(comic.issn).isEqualTo("issn")
        Truth.assertThat(comic.format).isEqualTo("format")
        Truth.assertThat(comic.description).isEqualTo("description")
        Truth.assertThat(comic.pageCount).isEqualTo(1)
        Truth.assertThat(comic.resourceURI).isEqualTo("resourceURI")
        Truth.assertThat(comic.serie).isNotNull()
        Truth.assertThat(comic.serie?.name).isEqualTo("name")
        Truth.assertThat(comic.serie?.resourceURI).isEqualTo("resourceURI")
        Truth.assertThat(comic.textObjects).isNotNull()
        Truth.assertThat(comic.textObjects.size).isEqualTo(0)
        Truth.assertThat(comic.urls).isNotNull()
        Truth.assertThat(comic.urls.size).isEqualTo(0)
        Truth.assertThat(comic.variants).isNotNull()
        Truth.assertThat(comic.variants.size).isEqualTo(0)
        Truth.assertThat(comic.collections).isNotNull()
        Truth.assertThat(comic.collections.size).isEqualTo(0)
        Truth.assertThat(comic.collectedIssues).isNotNull()
        Truth.assertThat(comic.collectedIssues.size).isEqualTo(0)
        Truth.assertThat(comic.dates).isNotNull()
        Truth.assertThat(comic.dates.size).isEqualTo(0)
        Truth.assertThat(comic.prices).isNotNull()
        Truth.assertThat(comic.prices.size).isEqualTo(0)
        Truth.assertThat(comic.images).isNotNull()
        Truth.assertThat(comic.images.size).isEqualTo(0)
        Truth.assertThat(comic.charactersSummary).isNotNull()
        Truth.assertThat(comic.charactersSummary.available).isEqualTo(1)
        Truth.assertThat(comic.charactersSummary.returned).isEqualTo(1)
        Truth.assertThat(comic.charactersSummary.collectionURI).isEqualTo("collectionURI1")
        Truth.assertThat(comic.charactersSummary.items).isNotNull()
        Truth.assertThat(comic.charactersSummary.items.size).isEqualTo(0)
        Truth.assertThat(comic.creatorsSummary).isNotNull()
        Truth.assertThat(comic.creatorsSummary.available).isEqualTo(2)
        Truth.assertThat(comic.creatorsSummary.returned).isEqualTo(2)
        Truth.assertThat(comic.creatorsSummary.collectionURI).isEqualTo("collectionURI2")
        Truth.assertThat(comic.creatorsSummary.items).isNotNull()
        Truth.assertThat(comic.creatorsSummary.items.size).isEqualTo(0)
        Truth.assertThat(comic.storiesSummary).isNotNull()
        Truth.assertThat(comic.storiesSummary.available).isEqualTo(3)
        Truth.assertThat(comic.storiesSummary.returned).isEqualTo(3)
        Truth.assertThat(comic.storiesSummary.collectionURI).isEqualTo("collectionURI3")
        Truth.assertThat(comic.storiesSummary.items).isNotNull()
        Truth.assertThat(comic.storiesSummary.items.size).isEqualTo(0)
        Truth.assertThat(comic.eventsSummary).isNotNull()
        Truth.assertThat(comic.eventsSummary.available).isEqualTo(4)
        Truth.assertThat(comic.eventsSummary.returned).isEqualTo(4)
        Truth.assertThat(comic.eventsSummary.collectionURI).isEqualTo("collectionURI4")
        Truth.assertThat(comic.eventsSummary.items).isNotNull()
        Truth.assertThat(comic.eventsSummary.items.size).isEqualTo(0)
    }

    @Test
    fun testTransformer_notEmpty_ItemsNotEmpty() {
        val characters = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name1", resourceURI = "resourceURI1")) }
        val creators = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name2", resourceURI = "resourceURI2")) }
        val stories = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name3", resourceURI = "resourceURI3")) }
        val events = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name4", resourceURI = "resourceURI4")) }
        val texts = arrayListOf<TextObject>().apply { add(TextObject(type = "type", language = "language", text = "text")) }
        val urls = arrayListOf<UrlItemResponse>().apply { add(UrlItemResponse(type = "type", url = "url")) }
        val variants = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name5", resourceURI = "resourceURI5")) }
        val collections = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name6", resourceURI = "resourceURI6")) }
        val collectedIssues = arrayListOf<ItemResponse>().apply { add(ItemResponse(name = "name7", resourceURI = "resourceURI7")) }
        val dates = arrayListOf<DateItemResponse>().apply { add(DateItemResponse(type = "type", date = "date")) }
        val prices = arrayListOf<PriceItemResponse>().apply { add(PriceItemResponse(type = "type", price = 3.6)) }
        val images = arrayListOf<MarvelThumbnail>().apply { add(MarvelThumbnail(path = "path1", extension = "extension1")) }

        val results = arrayListOf<MarvelComicOutputContract>()
        results.add(MarvelComicOutputContract(
                id = 1,
                digitalId = 1,
                title = "title",
                issueNumber = 1,
                variantDescription = "variantDescription",
                modified = "modified",
                isbn = "isbn",
                upc = "upc",
                diamondCode = "diamondCode",
                ean = "ean",
                issn = "issn",
                format = "format",
                description = "description",
                pageCount = 1,
                resourceURI = "resourceURI",
                series = ItemResponse(name = "name", type = "type", role = "role", resourceURI = "resourceURI"),
                thumbnail = MarvelThumbnail(path = "path", extension = "extension"),
                textObjects = texts,
                urls = urls,
                variants = variants,
                collections = collections,
                collectedIssues = collectedIssues,
                dates = dates,
                prices = prices,
                images = images,
                characters = ItemResponseList(available = 1, returned = 1, collectionURI = "collectionURI1", items = characters),
                creators = ItemResponseList(available = 2, returned = 2, collectionURI = "collectionURI2", items = creators),
                stories = ItemResponseList(available = 3, returned = 3, collectionURI = "collectionURI3", items = stories),
                events = ItemResponseList(available = 4, returned = 4, collectionURI = "collectionURI4", items = events))
                   )
        val outputContract = MarvelResponse(
                status = "status",
                attributionText = "attributionText",
                data = MarvelData(offset = 2, count = 4, results = results)
                                           )

        PowerMock.replayAll()
        val list = transformer.transformer(outputContract)

        val comic: Comic = list[0]

        Truth.assertThat(comic.textObjects).isNotNull()
        val text = comic.textObjects[0]
        Truth.assertThat(text.type).isEqualTo("type")
        Truth.assertThat(text.language).isEqualTo("language")
        Truth.assertThat(text.text).isEqualTo("text")

        Truth.assertThat(comic.urls).isNotNull()
        val url = comic.urls[0]
        Truth.assertThat(url.type).isEqualTo("type")
        Truth.assertThat(url.url).isEqualTo("url")

        Truth.assertThat(comic.variants).isNotNull()
        val variant = comic.variants[0]
        Truth.assertThat(variant.name).isEqualTo("name5")
        Truth.assertThat(variant.resourceURI).isEqualTo("resourceURI5")

        Truth.assertThat(comic.collections).isNotNull()
        val collection = comic.collections[0]
        Truth.assertThat(collection.name).isEqualTo("name6")
        Truth.assertThat(collection.resourceURI).isEqualTo("resourceURI6")

        Truth.assertThat(comic.collectedIssues).isNotNull()
        val collectedIssue = comic.collectedIssues[0]
        Truth.assertThat(collectedIssue.name).isEqualTo("name7")
        Truth.assertThat(collectedIssue.resourceURI).isEqualTo("resourceURI7")

        Truth.assertThat(comic.dates).isNotNull()
        val date = comic.dates[0]
        Truth.assertThat(date.type).isEqualTo("type")
        Truth.assertThat(date.date).isEqualTo("date")

        Truth.assertThat(comic.prices).isNotNull()
        val price = comic.prices[0]
        Truth.assertThat(price.type).isEqualTo("type")
        Truth.assertThat(price.price).isEqualTo(3.6)

        Truth.assertThat(comic.images).isNotNull()
        val image = comic.images[0]
        Truth.assertThat(image).isEqualTo("path1/standard_medium.extension1")

        Truth.assertThat(comic.charactersSummary.items).isNotNull()
        val character = comic.charactersSummary.items[0]
        Truth.assertThat(character.name).isEqualTo("name1")
        Truth.assertThat(character.resourceURI).isEqualTo("resourceURI1")

        Truth.assertThat(comic.creatorsSummary.items).isNotNull()
        val creator = comic.creatorsSummary.items[0]
        Truth.assertThat(creator.name).isEqualTo("name2")
        Truth.assertThat(creator.resourceURI).isEqualTo("resourceURI2")

        Truth.assertThat(comic.storiesSummary.items).isNotNull()
        val story = comic.storiesSummary.items[0]
        Truth.assertThat(story.name).isEqualTo("name3")
        Truth.assertThat(story.resourceURI).isEqualTo("resourceURI3")

        Truth.assertThat(comic.eventsSummary.items).isNotNull()
        val event = comic.eventsSummary.items[0]
        Truth.assertThat(event.name).isEqualTo("name4")
        Truth.assertThat(event.resourceURI).isEqualTo("resourceURI4")
    }

}
