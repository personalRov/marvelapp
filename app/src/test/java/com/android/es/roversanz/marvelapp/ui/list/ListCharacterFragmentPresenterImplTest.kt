package com.android.es.roversanz.marvelapp.ui.list

import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.ui.ListCharacterFragmentContract
import com.android.es.roversanz.marvelapp.ui.ListCharacterFragmentPresenterImpl
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.util.Date

@RunWith(PowerMockRunner::class)
@PrepareForTest(Character::class)
class ListCharacterFragmentPresenterImplTest {

    private lateinit var presenter: ListCharacterFragmentPresenterImpl
    private lateinit var view: ListCharacterFragmentContract.ListCharacterFragmentView

    @Before
    fun setUp() {
        view = PowerMock.createMock(ListCharacterFragmentContract.ListCharacterFragmentView::class.java)
        presenter = ListCharacterFragmentPresenterImpl()
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testPrepareView_ZeroItems() {
        val items = emptyList<Character>()
        view.showNoItemsVisibility(true)
        view.itemsUpdated(items)
        PowerMock.replayAll()
        presenter.prepareView(emptyList())
        Truth.assertThat(presenter.items).isEqualTo(items)
    }

    @Test
    fun testPrepareView_WithItems() {
        val character = getCharacter(1, Date(1502755200000))
        val characters = mutableListOf<Character>()
        characters.add(character)

        view.showNoItemsVisibility(false)
        view.itemsUpdated(characters)
        PowerMock.replayAll()
        presenter.prepareView(characters)
        Truth.assertThat(presenter.items).isEqualTo(characters)
    }

    @Test
    fun testOnRefresh() {
        val character = PowerMock.createMock(Character::class.java)
        view.refreshItems(0)

        PowerMock.replayAll()
        presenter.items.add(character)
        presenter.onRefresh()

        Truth.assertThat(presenter.items).isEmpty()
    }

    @Test
    fun testOnRefresh_withOffset() {
        val character = PowerMock.createMock(Character::class.java)

        view.refreshItems(20)
        PowerMock.replayAll()
        presenter.items.add(character)
        presenter.onRefresh(20)

        Truth.assertThat(presenter.items).isNotEmpty()
    }

    @Test
    fun testOnRefreshItems_withZeroprevius() {
        val character = PowerMock.createMock(Character::class.java)
        val characters = mutableListOf<Character>()
        characters.add(character)
        view.showNoItemsVisibility(false)
        view.itemsUpdated(characters)
        PowerMock.replayAll()
        presenter.onRefreshItems(characters)
    }

    @Test
    fun testOnRefreshItems_withNotZeroprevius() {
        val character = getCharacter(1, Date(1502755200000))
        val characters = mutableListOf<Character>()
        characters.add(character)

        val character2 = getCharacter(2, Date(1502755200000))
        val items = mutableListOf<Character>()
        items.add(character2)

        val itemsFinal = mutableListOf<Character>()
        itemsFinal.add(character2)
        itemsFinal.add(character)

        view.showNoItemsVisibility(false)
        view.itemsUpdated(itemsFinal)
        PowerMock.replayAll()
        presenter.items = items
        presenter.onRefreshItems(characters)
    }

    private fun getCharacter(id: Int, date: Date) = Character(
            id = id,
            name = "name",
            description = "description",
            modifiedDate = date,
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            comicsSummary = getSummaryList(),
            seriesSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())
}
