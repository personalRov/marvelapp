package com.android.es.roversanz.marvelapp.ui.list

import com.android.es.roversanz.marvelapp.data.model.SummaryItem
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(Comic::class)
class ListComicFragmentPresenterImplTest {

    private lateinit var presenter: ListComicFragmentPresenterImpl
    private lateinit var view: ListComicFragmentContract.ListComicFragmentView

    @Before
    fun setUp() {
        view = PowerMock.createMock(ListComicFragmentContract.ListComicFragmentView::class.java)
        presenter = ListComicFragmentPresenterImpl()
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testPrepareView_ZeroItems() {
        val items = emptyList<Comic>()
        view.showNoItemsVisibility(true)
        view.itemsUpdated(items)
        PowerMock.replayAll()
        presenter.prepareView(emptyList())
        Truth.assertThat(presenter.items).isEqualTo(items)
    }

    @Test
    fun testPrepareView_WithItems() {
        val comic = getComic(1)
        val comics = mutableListOf<Comic>()
        comics.add(comic)

        view.showNoItemsVisibility(false)
        view.itemsUpdated(comics)
        PowerMock.replayAll()
        presenter.prepareView(comics)
        Truth.assertThat(presenter.items).isEqualTo(comics)
    }

    @Test
    fun testOnRefresh() {
        val comic = PowerMock.createMock(Comic::class.java)
        view.refreshItems(0)

        PowerMock.replayAll()
        presenter.items.add(comic)
        presenter.onRefresh()

        Truth.assertThat(presenter.items).isEmpty()
    }

    @Test
    fun testOnRefresh_withOffset() {
        val comic = PowerMock.createMock(Comic::class.java)

        view.refreshItems(20)
        PowerMock.replayAll()
        presenter.items.add(comic)
        presenter.onRefresh(20)

        Truth.assertThat(presenter.items).isNotEmpty()
    }

    @Test
    fun testOnRefreshItems_withZeroprevius() {
        val comic = PowerMock.createMock(Comic::class.java)
        val comics = mutableListOf<Comic>()
        comics.add(comic)
        view.showNoItemsVisibility(false)
        view.itemsUpdated(comics)
        PowerMock.replayAll()
        presenter.onRefreshItems(comics)
    }

    @Test
    fun testOnRefreshItems_withNotZeroprevius() {
        val comic = getComic(1)
        val comics = mutableListOf<Comic>()
        comics.add(comic)

        val comic2 = getComic(2)
        val items = mutableListOf<Comic>()
        items.add(comic2)

        val itemsFinal = mutableListOf<Comic>()
        itemsFinal.add(comic2)
        itemsFinal.add(comic)


        view.showNoItemsVisibility(false)
        view.itemsUpdated(itemsFinal)
        PowerMock.replayAll()
        presenter.items = items
        presenter.onRefreshItems(comics)
    }

    private fun getComic(id: Int) = Comic(
            id = id,
            digitalId = (id + 1),
            title = "title",
            issueNumber = (id + 2),
            variantDescription = "variantDescription",
            description = "description",
            modifiedDate = "modifiedDate",
            isbn = "isbn",
            upc = "upc",
            diamondCode = "diamondCode",
            ean = "ean",
            issn = "issn",
            format = "format",
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            pageCount = 0,
            textObjects = emptyList(),
            urls = emptyList(),
            prices = emptyList(),
            dates = emptyList(),
            images = emptyList(),
            serie = SummaryItem(name = "name", resourceURI = "resourceURI"),
            variants = emptyList(),
            collections = emptyList(),
            collectedIssues = emptyList(),
            charactersSummary = getSummaryList(),
            creatorsSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())
}
