package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.SummaryItem
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.data.model.comic.Comic
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import org.easymock.EasyMock
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class DetailComicFragmentPresenterImplTest {

    private lateinit var presenter: DetailComicFragmentContract.DetailComicFragmentPresenter
    private lateinit var view: DetailComicFragmentContract.DetailComicFragmentView
    private lateinit var stringProvider: StringProvider

    @Before
    fun setUp() {
        view = PowerMock.createMock(DetailComicFragmentContract.DetailComicFragmentView::class.java)
        stringProvider = PowerMock.createMock(StringProvider::class.java)
        presenter = DetailComicFragmentPresenterImpl(stringProvider)
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testPrepareView() {
        val comic = getComic(1)

        EasyMock.expect(stringProvider.getString(R.string.creators_count, comic.creatorsSummary.available)).andReturn("creators_count")
        EasyMock.expect(stringProvider.getString(R.string.stories_count, comic.storiesSummary.available)).andReturn("stories_count")
        EasyMock.expect(stringProvider.getString(R.string.events_count, comic.eventsSummary.available)).andReturn("events_count")
        EasyMock.expect(stringProvider.getString(R.string.characters_count, comic.charactersSummary.available)).andReturn("characters_count")

        view.setComicTitle(comic.title)
        view.setComicDescription(comic.description)
        view.loadComicImage(comic.picture)
        view.setComicCreators("creators_count")
        view.setComicStories("stories_count")
        view.setComicEvents("events_count")
        view.setComicCharacters("characters_count")

        PowerMock.replayAll()

        presenter.prepareView(comic)
    }

    private fun getComic(id: Int) = Comic(
            id = id,
            digitalId = (id + 1),
            title = "title",
            issueNumber = (id + 2),
            variantDescription = "variantDescription",
            description = "description",
            modifiedDate = "modifiedDate",
            isbn = "isbn",
            upc = "upc",
            diamondCode = "diamondCode",
            ean = "ean",
            issn = "issn",
            format = "format",
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            pageCount = 0,
            textObjects = emptyList(),
            urls = emptyList(),
            prices = emptyList(),
            dates = emptyList(),
            images = emptyList(),
            serie = SummaryItem(name = "name", resourceURI = "resourceURI"),
            variants = emptyList(),
            collections = emptyList(),
            collectedIssues = emptyList(),
            charactersSummary = getSummaryList(),
            creatorsSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())

}
