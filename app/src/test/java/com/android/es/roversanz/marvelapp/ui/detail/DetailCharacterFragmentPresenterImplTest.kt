package com.android.es.roversanz.marvelapp.ui.detail

import com.android.es.roversanz.marvelapp.R
import com.android.es.roversanz.marvelapp.data.model.character.Character
import com.android.es.roversanz.marvelapp.data.model.SummaryList
import com.android.es.roversanz.marvelapp.utils.strings.StringProvider
import org.easymock.EasyMock
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.modules.junit4.PowerMockRunner
import java.util.Date

@RunWith(PowerMockRunner::class)
class DetailCharacterFragmentPresenterImplTest {

    private lateinit var presenter: DetailCharacterFragmentContract.DetailCharacterFragmentPresenter
    private lateinit var view: DetailCharacterFragmentContract.DetailCharacterFragmentView
    private lateinit var stringProvider: StringProvider

    @Before
    fun setUp() {
        view = PowerMock.createMock(DetailCharacterFragmentContract.DetailCharacterFragmentView::class.java)
        stringProvider = PowerMock.createMock(StringProvider::class.java)
        presenter = DetailCharacterFragmentPresenterImpl(stringProvider)
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun testPrepareView() {
        val character = getCharacter(Date(1502755200000))

        EasyMock.expect(stringProvider.getString(R.string.comics_count, character.comicsSummary.available)).andReturn("character_comics_count")
        EasyMock.expect(stringProvider.getString(R.string.stories_count, character.storiesSummary.available)).andReturn("character_stories_count")
        EasyMock.expect(stringProvider.getString(R.string.events_count, character.eventsSummary.available)).andReturn("character_events_count")
        EasyMock.expect(stringProvider.getString(R.string.series_count, character.seriesSummary.available)).andReturn("character_series_count")

        view.setCharacterName("name")
        view.setCharacterDescription("description")
        view.setCharacterDate("15/08/2017")
        view.loadCharacterImage("picture")
        view.setCharacterComics("character_comics_count")
        view.setCharacterStories("character_stories_count")
        view.setCharacterEvents("character_events_count")
        view.setCharacterSeries("character_series_count")

        PowerMock.replayAll()

        presenter.prepareView(character)
    }

    private fun getCharacter(date: Date) = Character(
            id = 1,
            name = "name",
            description = "description",
            modifiedDate = date,
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            comicsSummary = getSummaryList(),
            seriesSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())

}
